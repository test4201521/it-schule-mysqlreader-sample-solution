﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Diagnostics;

namespace MySQLReaderSampleSolution
{
    class DBConnector
    {
        // Attribut für die MySqlConnection unseres DBConnectors
        private readonly MySqlConnection connection;

        // Diese zwei Attribute machen einfach überhaupt keinen Sinn und sind maximal unnötig WTF
        //private MySqlCommand? command;
        //private MySqlDataReader? result;

        // Konstruktor für diese Klasse DBConnector
        public DBConnector(string server, string database, string user, string password) {
            // Inizialisieren des Attributs connection durch Konstruktor-Aufruf von MySqlConnection
            // MySqlConnection benötigt einen ConnectionString im folgenden format:
            // Beispiel: "server=localhost;database=baum;userid=peterpan;password=secretpw"
            // Das $ vor dem String sagt aus, dass ein formatierter String folgt
            // Dadurch kann man in den geschweiften Klammern {} direkt Variablen angeben :)
            connection = new MySqlConnection($"server={server};database={database};userid={user};password={password}");
        }

        // Hilfsfunktion die als Boolean zurückgibt, ob die Verbindung geöffnet ist
        public bool IsOpen() {
            return connection.State == ConnectionState.Open;
        }

        public void Open() {
            if (!IsOpen()) {
                // Versuche die Verbindung zu öffnen, sofern sie es nicht schon ist
                try {
                    connection.Open();
                } catch (MySqlException ex) {
                    Trace.WriteLine(ex);
                }
            }
        }

        public void Close() {
            // Schließt die Verbindung, vorausgesetzt sie ist überhaupt geöffnet
            if (IsOpen()) connection.Close();
        }
        public MySqlDataReader? Execute(string queryString) {
            // Teste ob die Verbindung offen, und der queryString nicht leer ist (sonst gibts Error)
            if (!IsOpen() || queryString == null || queryString == "") return null;
            // Erstelle einen neuen MySqlCommand mit unserem queryString und connection und gebe den zugehörigen Reader zurück
            return new MySqlCommand(queryString, connection).ExecuteReader();
        }
        public DataTable? ExecuteTable(string queryString) {
            // Hole mit der Execute() Methode den Reader anhand vom übergebenen String
            var reader = Execute(queryString);
            if (reader == null) return null;
            // Erstelle einen neuen DataTable
            var table = new DataTable();
            // Lade die Daten aus dem Reader in den DataTable
            table.Load(reader);
            // Schließe den Reader
            reader.Close();
            return table;
        }
    }
}
