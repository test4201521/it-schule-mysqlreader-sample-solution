﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace MySQLReaderSampleSolution {
    public partial class MainWindow : Window {

        private DBConnector? dbConnector;
        private readonly Stopwatch stopwatch = Stopwatch.StartNew();
        public MainWindow() {
            InitializeComponent();
        }

        private void OnSaveButtonClick(object sender, RoutedEventArgs e) {
            // Auslesen der ganzen Parameter-Felder sobald der Save-Button gedrückt wird
            string server = ServerField.Text;
            string database = DatabaseField.Text;
            string user = UserField.Text;
            string password = PasswordField.Password;
            // Hier kann/sollte? man die eingegebenen Verbindungsdaten noch verifizieren
            dbConnector = new DBConnector(server, database, user, password);
            dbConnector.Open();
            Trace.WriteLine($"Tried to open connection. Success was: {dbConnector.IsOpen()}");
        }

        // Diese Me
        private void OnQueryButtonClick(object sender, RoutedEventArgs e) {
            string query = QueryField.Text;
            try {
                switch (ResultTabControl.SelectedIndex) {
                    // Wenn im TabControl aktuell die Text-Ansicht geöffnet ist:
                    case 0:
                        GetTextData(query);
                        break;
                    // Wenn im TabControl aktuell die DataGrid-Ansicht geöffnet ist:
                    case 1:
                        GetTableData(query);
                        break;
                }
                // Wenn bis hier kein Error, zeige als QueryInfo in grüner Schrift die Ausführungszeit in ms an,
                // welche von der Stopuhr gemessen wurde
                QueryLabel.Foreground = Brushes.Green;
                QueryInfoLabel.Content = $"Execution time: {stopwatch.ElapsedMilliseconds}ms";
            }
            catch (Exception ex) {
                // Wenn ein Error vorkam, setze die Farbe auf Rot und zeige den Error an
                QueryLabel.Foreground = Brushes.Red;
                QueryInfoLabel.Content = $"MySQL-Error: {ex.Message}";
            }
        }

        private void GetTextData(string query) {
            stopwatch.Restart();
            // DataTable anhand des übergebenen Query-Strings vom DBConnector via ExecuteTable() holen
            DataTable? dataTable = dbConnector!.ExecuteTable(query);
            stopwatch.Stop();
            if (dataTable == null) return;
            string resultString = "";
            // Erstellen der ersten Reihe mit den Spaltennamen
            foreach (DataColumn col in dataTable.Columns) {
                resultString += col.ColumnName + " | ";
            }
            // Hinzufügen der ---- Trennlinie mit Länge so lang wie der bisherige String (Key Values)
            resultString += "\n" + new string('-', resultString.Length) + "\n";
            // Iteriere über jede Reihe im DataTable
            foreach (DataRow row in dataTable.Rows)
            {
                // Iteriere über jedes Item in der aktuellen Reihe
                foreach (var item in row.ItemArray) {
                    // Hinzufügen des aktuellen Items zum Gesamt-String mit 3 Leerzeichen als Trennung
                    resultString += item!.ToString() + "   ";
                }
                // Neue Zeile für neue Reihe
                resultString += "\n";
            }
            // Setzen der Ergebnis-TextBox auf den generierten String
            ResultTextBox.Text = resultString;
        }

        private void GetTableData(string query) {
            stopwatch.Restart();
            // Holen des Reader aus dem DBConnector
            MySqlDataReader? reader = dbConnector!.Execute(query);
            if (reader == null) return;
            // Setzen des UI DataGrids auf die Daten aus der SQL-Query
            ResultDataGrid.ItemsSource = reader;
            stopwatch.Stop();
        }

        // Kleine Hilfsmethode die einen DataTable in einen SCHÖN formattierten String zurückgibt :)

        /*private string ToFormattedString(DataTable dataTable) {
            int columnPadding = 2;
            // Since .PadRight() is used, it is important to use a monospaced font for the formatting to work right!
            int[] maxLengths = new int[dataTable.Columns.Count];
            for (int i = 0; i < dataTable.Columns.Count; i++) {
                maxLengths[i] = dataTable.Columns[i].ColumnName.Length;
                foreach (DataRow row in dataTable.Rows) {
                    if (row[i] == null) continue;
                    maxLengths[i] = Math.Max(maxLengths[i], row[i].ToString()!.Length);
                }
            }
            string header = "";
            for (int i = 0; i < dataTable.Columns.Count; i++) {
                header += dataTable.Columns[i].ColumnName.PadRight(maxLengths[i] + columnPadding);
            }
            string divider = new string('-', maxLengths.Sum() + (maxLengths.Length - 1) * columnPadding);
            var rows = new List<string>();
            foreach (DataRow row in dataTable.Rows) {
                string currentRow = "";
                for (int i = 0; i < dataTable.Columns.Count; i++) {
                    if (row[i] == null) continue;
                    currentRow += row[i].ToString()!.PadRight(maxLengths[i] + columnPadding);
                }
                rows.Add(currentRow);
            }
            return header + "\n" + divider + "\n" + string.Join("\n", rows);
        }*/
    }
}
